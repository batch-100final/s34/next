// base imports
import { useContext, useState, useEffect } from 'react';
import Router from 'next/router';

import { Form, Button, Container } from 'react-bootstrap';

import UserContext from '../../UserContext';

import usersData from '../../data/userdata';

export default function Login() {
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isDisabled, setIsDisabled] = useState(true);

     function login(e) {
        // authentication based on imported users data
        const match = usersData.find((user) => {
            return user.email === email && user.password === password;
        })

        if (match) {
            localStorage.setItem('email', email);
            // this will be converted to be a string once it is set in the local
            localStorage.setItem('isAdmin', match.isAdmin);

            // set the global user state to have properties obtained from local storage
            setUser({
                email: localStorage.getItem('email'),
                isAdmin: match.isAdmin
            });
            Router.push('/courses');

        } else {
            alert('Authentication failed, no matched users found')
        }

         // prevents page redirection
        e.preventDefault();
        console.log(user);
        // clear the input fields
        setEmail('');
        setPassword('');
	}

	  useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';

        //Determine if all conditions have been met
        if (isEmailNotEmpty && isPasswordNotEmpty) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password]);

	  useEffect(() => {
        console.log(`User with an email: ${email} is an admin`)
    }, [user.isAdmin, email]) //user.email


	return (
		<Container fluid className="my-5">
                <h3>Login</h3>
                <Form onSubmit={login}>
                    <Form.Group>
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email} 
                            onChange={(e) => setEmail(e.target.value)} 
                            required
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value={password} 
                            onChange={(e) => setPassword(e.target.value)} 
                            required
                        />
                    </Form.Group>
                    <Button variant="success" type="submit" disabled={isDisabled}>
                        Login
                    </Button>
                </Form>
            </Container>
	)
}