// base imports
import {Fragment, useContext} from 'react';
import Head from 'next/head';
// bootstrap
import { Table, Button } from 'react-bootstrap';

// app components
import CourseCard from '../../components/CourseCards';

// app data
import coursesData from '../../data/coursesdata';

//
import UserContext from '../../UserContext';

export default function Courses(){
	// use the UserContext and destructure it to access the user state defined in app component
	const { user } = useContext(UserContext);

	// table rows to be rendered in a bootstrap table when an admin is logged in
	const courseRow = coursesData.map((indivCourse) => {
		return (
			<tr key={indivCourse.id}>
				<td>{indivCourse.id}</td>
				<td>{indivCourse.name}</td>
				<td>{indivCourse.description}</td>
				<td>{indivCourse.price}</td>
				<td>{indivCourse.onOffer ? 'open' : 'closed'}</td>
				<td>{indivCourse.start_date}</td>
				<td>{indivCourse.end_date}</td>
				<td>
					<Button variant="warning">Update</Button> 
					<Button variant="danger" >Disable</Button>
					
				</td>
			</tr>
		)
	})

	const courses = coursesData.map((indivCourse) => {
		
			return (
				<CourseCard key = {indivCourse.id} courseProp = {indivCourse} />
			)
	})


	return(
		user.isAdmin === true 
		?
		<Fragment>
			<Head>
				<title>Courses admin dashboard</title>
			</Head>
				<h1>Courses Dashboard</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Course ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>On Offer</th>
							<th>Start Date</th>
							<th>End Date</th>
						</tr>
					</thead>
					<tbody>
						{courseRow}
					</tbody>
				</Table>
		</Fragment>
		:
		<Fragment>
			{courses}
		</Fragment>
	)
}

