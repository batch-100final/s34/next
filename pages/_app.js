import React, { useState, useEffect } from 'react';
import '../styles/globals.css'

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

import Navbar from '../components/Navbar';

// import Context provider
import { UserProvider } from '../UserContext';

export default function MyApp({ Component, pageProps }) {
	const[user, setUser] = useState({
		email: null,
		isAdmin: null
	})

	useEffect(() => {
		setUser({
			email: localStorage.getItem('email'),
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})
	}, [])

	//effect hook for testing the 
	useEffect(() => {
		console.log(user.email);
	}, [user.email])

	// function for clearing local storage upon logout
	const unsetUser = () => {
		localStorage.clear();

		setUser({
			email: null,
			isAdmin: null
		})
	}

  return(
  	<React.Fragment>
  		<UserProvider value={{user, unsetUser, setUser}}>
  			<Navbar />
  				<Container>
  					<Component {...pageProps} />
  				</Container>
  		</UserProvider>
  	</React.Fragment>
  ) 
}


